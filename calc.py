from math import *
from collections import namedtuple
Pair = namedtuple("Pair", ["first", "second"])
Triple = namedtuple("Triple", ["first", "second", "third"])


def moyenne(tableau):
    return sum(tableau, 0.0) / len(tableau)

def variance(tableau):
    m = moyenne(tableau)
    return moyenne([(x-m)**2 for x in tableau])

def ecartype(tableau):
    return variance(tableau)**0.5

def volatility(x, nb):
    return ecartype(x) * sqrt (nb) 

def period_performance(value_start, value_end):
    return (value_end - value_start) / value_start

def return_investissment(value_start, value_end, nbDay):
    return (1 + period_performance(value_start, value_end))**(365/nbDay) - 1

def sharpe(value_start, value_end, nbDay): 
    ref_comparaison = 0.5
    tableau = [2,3,5,8]
    num_years = 5
    return ((return_investissment(value_start, value_end, nbDay) - 
        ref_comparaison) / volatility(tableau, num_years)) 

def covariance(variation_x, variation_y):
    result = 0
    moyenne_x = moyenne(variation_x)
    moyenne_y = moyenne(variation_y)
    n = min(len(variation_x), len(variation_y))
    for i in range(0,n) : 
        result = result + ((variation_x[i] - moyenne_x) * (variation_y[i] - moyenne_y))
    return result / n

def rentability(last_value, new_value, income):
    return ((new_value - last_value) + income) / last_value

""" Liste de paires"""

def variance_portfolio(actif_list, matrix):
    result_final = 0
    result_b = 0
    n = len(actif_list)
    """for name, proportion, variation_list in actif_list :
        result_a = result_a + (proportion**2 * ecartype(variation_list))"""
    """for name_a, proportion_a in actif_list :
        for name_b, proportion_b in actif_list :"""
    value = actif_list.first
    prop = actif_list.second
    for i in range (0, len(value)):
        for j in range (0, len(value)):
            name_a = int(value[i])
            name_b = int(value[j])
            result_final = result_final + ((prop[i]) * (prop[j]) * matrix[name_a][name_b])
    return result_final

def rendement_portfolio(actif_list, proportion_list, rendement_list) :
    result = 0
    for i in range(0, len(actif_list)) :
        result = result + (proportion_list[i] * rendement_list[int(actif_list[i])])
    return result

def tri_array(tableau) :
    tab_size = len(tableau)
    i = 0
    tmp = 0
    j = 0
    for i in range (0,tab_size) :
        for j in range(i+1, tab_size) :
            if ((tableau[j].first) < (tableau[i].first)) :
                tmp = tableau[i]
                tableau[i] = tableau[j]
                tableau[j] = tmp
    return tableau

def max_float(tableau) :
    maximum = 0
    for i in range (0, len(tableau)):
        if (tableau[i] > maximum):
            maximum = tableau[i]
    return maximum

def return_best(tableau) :
    maximum = 0
    place = 0
    for i in range (0, len(tableau)):
        if (tableau[i] > maximum):
            maximum = tableau[i]
            place = i
        return place
        

def marcowitz_algo(list_portfolio = [Pair([], [])], benchmark = None, rendement_list = None, matrix = None):
    ref_comparaison = 0.5
    list_rentability_ok = []
    list_variance = []
    true_variance = []
    list_final = []
    tmp = []
    rend2_list = []
    final_list = []
    actif_list = []
    prop_list = []
    port_folio = []
    supp_list = []
    supp2_list = []
    max_variance = []
    copy_variance = []
    variance_recap = []
    mean_variance = 0
    total_variance = 0
    n = 0
    variance_weight = 0
    rentability_weight = 0
    rend_portfolio = 0
    variance_value = 0
    rend_value = 0
    maximum = 0
    place = 0
    """print (len(list_portfolio))"""
    for i in range(0, len(list_portfolio)):
        """for actif_list, prop_list in list_portfolio"""
        actif_list = list_portfolio[i].first
        prop_list = list_portfolio[i].second
        rend_portfolio = rendement_portfolio(actif_list, prop_list, rendement_list)
        if (rend_portfolio > 0.5):
            Pair_rendement = Pair(rend_portfolio, list_portfolio[i])
            list_rentability_ok.append(Pair_rendement)
    for i in range(0, len(list_rentability_ok)):
        var_portfolio = variance_portfolio(list_rentability_ok[i].second, matrix)
        """print ("variance : " +  str(var_portfolio))
        print ("list_portfolio : " + str(list_portfolio[i])) """
        """Triple_value = Triple(var_portfolio, list_rentability_ok, list_portfolio[i])"""
        """list_variance.append(Triple_value)"""
        variance_recap.append(var_portfolio)
        """rajout """
    for i in range (0, 20):
        maximum = max_float(variance_recap)
        place = return_best(variance_recap)
        Triple_value = Triple(maximum, list_rentability_ok[place], list_portfolio[place])
        max_variance.append(Triple_value)
        del variance_recap[place]
    port_folio = max_variance
    print ("port_folio : " + str(port_folio))
    """tri_array(list_variance)"""
    """
    n = 20
    for i in range(0, n) :
        port_folio.append(list_variance[i])
        total_variance = total_variance + list_variance[i].first
    mean_variance = total_variance / n
    print (mean_variance)"""
    for i in range(0, len(port_folio)) :
        print("port_folio :" + str(len(port_folio)))
        variance_value = port_folio[i].first
        rend_value = port_folio[i].second
        for j in range(i+1, len(port_folio)):
            if (port_folio[j].first == variance_value) :
                if (port_folio[j].second < rend_value) : 
                    supp_list.append(port_folio[i].third)
            if (port_folio[j].second == rend_value) :
                if (port_folio[j].first > variance_value) :
                    supp_list.append(port_folio[j].third)
    for elem in supp_list :
        if elem not in supp2_list :
            supp2_list.append(elem)
    for elem in port_folio[i].third :
        if elem not in supp2_list :
            final_list.append(elem)
    print("final list : " + str(i) + " " + str(len(final_list)) + str(final_list))
    print("supp 2 list : " + str(i) + " " +  str(len(supp2_list)))
    """for i in range (0, len(supp_list)):
    print ("final_list : " + str(len(final_list)))"""
    return final_list

def transformpercent(assets, proportions, data_on_assets) :
    price = 0
    quantity = []
    total_money = 500000
    value_actif = 0
    for i in range (0, len(assets)) :
        price = data_on_assets[assets[i]]
        print("price :" + str(price))
        print("proportion : " + str(proportions[i]))
        print("total money : " + str(total_money))
        value_actif = (float(proportions[i]) * (float(total_money))) / float(price.split(' ')[0].replace(',', '.'))
        print("value actif : " + str(value_actif))
        quantity.append((int(value_actif)))
    return quantity

def transformation_currency(data_on_assets) :
    total = []
    int_price = 0
    string_price = ""
    string2_price = ""
    new_price = 0
    currency_string = ""
    chiffre_list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ','] 
    for i in range (0, len(data_on_assets) - 1) :
        total = data_on_assets[i]
        price = []
        currency = []
        for j in range (0, len(total)):
            if (total[j] in chiffre_list):
                price.append(total[j])
            else :
                if (total[j] != " "):
                    currency.append(total[j])
        currency_string = "".join(currency)
        string_price = "".join(price)
        string2_price = string_price.replace(',','.')
        int_price = float(string2_price)
        if (currency_string == 'D'):
            new_price = int_price * (0.773514851485)
        elif (currency_string == "DKK"):
            new_price = int_price * (0.1345)
        elif (currency_string == "SF"):
            new_price = int_price * (0.822774395261)
        elif (currency_string == "SEK"): 
            new_price = int_price * (0.11201)
        elif (currency_string == "GBP"):
            new_price = int_price * (1.199616122841)
        elif (currency_string == "Y"):
            new_price = int_price * (0.0100542)
        elif (currency_string == "NOK"):
            new_price = int_price * (0.1291)
        else:
            new_price = int_price
        data_on_assets[i] = str(new_price) + " E"

def check_validity_per_quantity(actifstypes, quantityactifs, actifdata):
    total = 0.
    if (len(quantityactifs) != len(actifstypes)):
        print("Very bad, actiftypes et quantityactifs de types différents ")
        return False
    if (len(actifstypes) != 20 ):
        print("Portefeuille non composé de 20 actifs, ou mauvais parametres.")
        #return False #just a warning while test
    total_money = 0.
    for i in range(0, len(actifstypes)):
        total_money = total_money + quantityactifs[i] * float(actifdata[actifstypes[i]].split(' ')[0].replace(',', '.'))
    print("Total money is " + str(total_money))
    for i in range(0, len(actifstypes)):
        money_curr_actif = quantityactifs[i] * float(actifdata[actifstypes[i]].split(' ')[0].replace(',', '.'))
        proportion_curr = (money_curr_actif / total_money)

        print("proportion actif " + str(actifstypes) + " is " + str(proportion_curr))

    print("DONE")
        #while money_curr_actif / total_money < 10:



def check_validity(portfolio = [Pair([], [])]):
    list_actif = portfolio.first
    list_prob = portfolio.second
    if (len(portfolio) != 20):
        print("Le portfeuille n'est pas composé de 20 actifs!! Il en comporte :" + str(len(portfolio)))
    for i in range (0, len(portfolio.first)):
        if (list_prob[i] < 0.01):
            print ("L'actif : " + list_actif[i] + " est en dessous de 1% de NAV! ATTENTION!")
        if (list_prob[i] > 0.1):
            print ("L'actif : " + list_actif[i] + " est au dessus de 10% de NAV! ATTENTION!")




"""
def marcowitz_algo(): 1) trouver les portefeuilles de variance minimale qui s'atisfait l'objectif de rentabilité
2) Garder celui qui pour une variance donné donne le rendement le plus élevé
"""
"""s = volatility([2,3,5,8], 5)"""
"""s = return_investissment(5000, 5738, 304)"""
"""s = period_performance(5000, 5738)"""
"""s = sharpe(5200, 5700, 30)"""

"""s = covariance([-0.0517, 0.1587, 0.2477, -0.0215, -0.1749, 0.0333, 0.0439, -0.1377, -0.0429, 
    0.1525, -0.1323, -0.1324], [-0.1897, -0.0297, 0.2061, 0.1662, -0.1578, 0.0096, 0.0417, 
        0.0755, -0.1708, 0.1927, 0.0171, -0.1706])"""

"""s = tri_array([7,4,11,5,23])"""

"""s = rentability(50, 60, 5)"""

"""s = variance_portfolio([("IBM", 20)], [[0.4, 0.66][0.77, 0.54]])"""

"""s = marcowitz_algo([7, 6, 2], [4, 5, 6], [6, 4, 1], [11, 9, 2])"""

"""s = max_float([7.5, 3.3, 1.8, 11.5, 17.8, 12.2, 9.9])"""

#print(s)
