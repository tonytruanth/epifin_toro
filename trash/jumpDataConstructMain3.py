from jump import *
from calc import *

# DRAFT


# APPLIES ASSET
dateset = ['2012-01-02', '2012-06-02', '2013-01-02', '2013-06-02', '2014-01-02', '2014-06-02', '2015-01-02', '2015-06-02', '2016-01-02', '2016-06-02', '2017-01-02', '2017-06-02', '2018-01-02', '2018-06-02']
# pour les analyses approfondies de long échantillon de données
#dateset = ['2012-01-02', '2018-08-31']
asset_datas = []

for i in range(0, len(dateset)):
    s = get_data("/asset", None, False, ["ASSET_DATABASE_ID", "LABEL", "TYPE", "LAST_CLOSE_VALUE_IN_CURR"], dateset[i],
                 '', '')  # tous les assets avec ces deux champs en particulier
    res = parse_asset(parse_fields(s, 4), s)
    # print(res) #TEST: Pandas-dataframe
    asset_datas.append(res)

# APPLIES RATIOS (SHARPE, COVAR FOR NOW)
'''
#Fonction Romain qui ne marche pas
# asset_datas[0]["SHARPE"] = ""
for j in range(0, 365):
    if asset_datas[0]['ASSET_DATABASE_ID'][j] != '1031':
        for i in range(0, len(asset_datas) - 1):
            sharpeActuelActif = str(
                sharpe(float(asset_datas[i]['LAST_CLOSE_VALUE_IN_CURR'][j].replace(',', '.').split(' ')[0]),
                       float(asset_datas[i + 1]['LAST_CLOSE_VALUE_IN_CURR'][j].replace(',', '.').split(' ')[0]), 180 * 5 + 90))
            print("SHARPE " + asset_datas[i]['LABEL'][j] + " IS " + sharpeActuelActif)
        # asset_datas[0]['SHARPE'][j] = sharpeActuelActif
        # TEST
'''

# TODO convert all in euros... Si on a le temps lol...
'''
asset_datas[0]["INDEX"] = "" #new
asset_datas[0]["SHARPE"] = ""
asset_datas[0]["RENDEMENT"] = ""
for i in range(0, 365):
    sharpejson = compute_ratios(20, asset_datas[0]['ASSET_DATABASE_ID'][i], None, '2012-01-02', '2018-08-31')
    sharpe = json.loads(sharpejson).get(asset_datas[0]['ASSET_DATABASE_ID'][i], {})['20']['value'].replace(',', '.')
    asset_datas[0]['SHARPE'][i] = sharpe
    asset_datas[0]['INDEX'][i] = i
    #print("SHARPE " + asset_datas[0]['LABEL'][i] + " IS " + sharpe)
    #TOTEST
    rendtjson = compute_ratios(21, asset_datas[0]['ASSET_DATABASE_ID'][i], None, '2012-01-02', '2018-08-31')
    rendement = json.loads(rendtjson).get(asset_datas[0]['ASSET_DATABASE_ID'][i], {})['21']['value'].replace(',', '.')
    asset_datas[0]['RENDEMENT'][i] = rendement
    #print(rendement)

#asset_datas[0].to_csv('0.csv')
#asset_datas[1].to_csv('1.csv')
'''

for i in range(1, len(asset_datas)):
    asset_datas[i].to_csv("resources/" + str(i) + '.csv')

# COVAR = CALCUL INFINISSABLE
assetIdList = asset_datas[0]['ASSET_DATABASE_ID']
corelations = [[0. for i in range(0, 364)] for j in range(0, 364)]
print(corelations)
for i in range(187, 200):
    for j in range(0, 364):
        if i != j and assetIdList[i] != '1031' and assetIdList[j] != '1031': #1031 = porefeuille
            coreljson = compute_ratios(19, asset_datas[0]['ASSET_DATABASE_ID'][i],
                                       asset_datas[0]['ASSET_DATABASE_ID'][j], '2012-01-02', '2018-08-31')
            corelationIJ = json.loads(coreljson).get(asset_datas[0]['ASSET_DATABASE_ID'][i], {})['19']['value'].replace(
                ',', '.')
            partition = corelationIJ.partition('.')
            if corelationIJ[0] == '-' or (corelationIJ[0] >= '0' and corelationIJ[0] <= '9'):
                    print(str(i) + " " + str(j) + " IS " + corelationIJ)
                    newelement = float(corelationIJ)
                    corelations[i][j] = float(corelationIJ)
            else:
                corelations[i][j] = 0. #ERROR THO?
                print("KO conversion of" + asset_datas[0]['LABEL'][i] + " to " + asset_datas[0]['LABEL'][j])
        else:
            corelations[i][j] = 0.
    numpyexportcsv = np.asarray(corelations)
    np.savetxt("corellations" + str(i) + ".csv", numpyexportcsv, fmt="%.15f", delimiter=",")
# TODO CALL FOR UPDATE PORTFOLIO
# PROCESS = lancez API CALL
numpyexportcsv = np.asarray(corelations)
np.savetxt("corellations.csv", numpyexportcsv, fmt= "%.12f", delimiter=",")

# json.parse pour transformer ca en json à float
print("SHARPE IS " + sharpe)
print("42")
while (1):
    print("finished")

# TACHES

'''
s = get_data("/asset", None, False, ["ASSET_DATABASE_ID", "LABEL", "TYPE", "LAST_CLOSE_VALUE_IN_CURR"], '2012-01-02', '', '') #tous les assets avec ces deux champs en particulier
#s = get_data("/asset/769", None, False, ["ASSET_DATABASE_ID", "LABEL", "TYPE", "LAST_CLOSE_VALUE_IN_CURR"], '', '', '') #les quatre champs en particulier de l'asset 769
print(s)
res = parse_asset(parse_fields(s, 4), s)
print(res) #TEST: Pandas-dataframe
#print(res['ASSET_DATABASE_ID'][5]) #if u want one value
'''
