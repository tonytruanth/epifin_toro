import requests
import json
import pandas as pd
import numpy as np
import datetime
#DE MEMOIRE? viser portefeuille SHARPS ENTRE 10 ET 20
#Bon SHARP certain diront >1 certain diront >5...
#PARTIE ASSET
#URL = 'https://hostname:port/api/v1/'
URL = 'https://dolphin.jump-technology.com:3472/api/v1'
AUTH = ('epita_user_4', 'vRkt9KhQZThs5hQg')


def columns_to_str(c, datenow, datebegin, dateend):
	s = "?"
	for i in range (0, len(c)):
		if i == 0:
			s = s + "columns=" + c[i]
		else:
			s = s + "&columns=" + c[i]
	if (datenow != ''):
		s = s + '&date=' + datenow
	return s

def get_data(endpointApi, date=None, full_response=False, columns=list(), datenow='', datebegin='', dateend=''):
 payload = {'date': date, 'fullResponse': full_response }
 res = requests.get(URL + endpointApi + columns_to_str(columns, datenow, datebegin, dateend),
 params=payload,
 auth=AUTH,
 verify=False)
 return res.content.decode('utf-8')

# input : le détail d'un /asset (tous les assets)
# output : une list de ses fields
def parse_fields(string, nb):
	s = []
	for i in range (0, nb):
		s.append(string.partition('":{\"')[0])
		string = string.partition('":{\"')[2]
		if i == 0:
			s[i] = s[i].partition('\"')[2]
		else:
			s[i] = s[i].partition('\"},\"')[2]
	return s

def parse_asset(fields, string):
#on va parser le string, stocker les données en format array (un array pour les types, et un array par ligne) et les stocker via pandas
	s = [[]]
	for j in range (0, 365):
		s.append([])
		for i in range (0, len(fields)):
			s[j].append(string.partition('value\":\"')[2])
			string = s[j][i]
			s[j][i] = s[j][i].partition('\"')[0]
			if string[string.find("}")+1] == '}' and i < len(fields) - 1: #in case one field missing
				break
	#res = res.append(s, ignore_index=True)
	res = pd.concat([pd.DataFrame(s) for i in range(1)], ignore_index=True)
	res.columns = fields;
	return res

#PARTIE RATIOS (notamment corélation vu que Sharpe on calcule nous mêmes)

def compute_ratios(ratio_ids, asset_ids, benchmark=None, start_date=None, end_date=None):
    """
    Compute the given ratios for the given assets
    :param ratio_ids: The ratios array to compute
    :param asset_ids: The asset to compute the ratios from
    :param benchmark: The benchmark ?
    :param start_date: The date to start computation at
    :param end_date: The date to end computation at
    :return: The ratios results
    """
    res = requests.post(URL + '/ratio/invoke',
                        auth=AUTH,
                        verify=False,
                        data=json.dumps({
                                'ratio': [ratio_ids],
                                'asset': [asset_ids],
                                'benchmark': benchmark,
                                'start_date': start_date,
                                'end_date': end_date,
                                'frequency': None
                        }))
    return res.content.decode('utf-8')

def update_portfolio(data):
	print("data is ")
	print(data)
	payload = {'date': '2012-02-01'}
	res = requests.put(URL + '/portfolio/1031/dyn_amount_compo', auth=AUTH, data=json.dumps(data))
	return res.content.decode('utf-8')