from jump import *
from calc import *
from collections import namedtuple
from numpy import transpose
import pandas as pd
import numpy as np


def submitPortfolio(portfolioIndexes, portfolioQuantities, data_on_assets): #dans data_on_assets on s'interesse notamment au correspondance INDEX/ASSET_DATABASE_ID
    portfolioTrueIds = []
    for i in range (0, len(portfolioIndexes)):
        portfolioTrueIds.append(data_on_assets['ASSET_DATABASE_ID'][int(portfolioIndexes[i])])
    #if (len(portfolioTrueIds) != len(portfolioQuantities)):
    #    print("ERROR IN MATCH QUANTITIES AND INDEXES SIZE")
    print(portfolioTrueIds)
    assets = []
    for i in range (0, len(portfolioIndexes)):
        assets.append({'asset': {'asset': portfolioTrueIds[i], 'quantity': portfolioQuantities[i]}})
    print(assets)
    submissionData = {
        'label': 'epita_ptf_4',
        'currency' : {'code' : 'EUR'},
        'type' : 'front',
        'values' : {'2012-01-02' : assets}
    }
    print(update_portfolio(submissionData))

Pair = namedtuple("Pair", ["first", "second"])
nbactifsretenus = 10 #for fast testing sake 10/2 or 10/3 but it will be 15/3 for real situation test and 20 with scale = 2for submit
treescale = 2

# PARTIE PORTEFEUILLE
portfoliorepartitions = []
portfoliorepartition = []
portfoliorepartition2 = []
portfoliorepartition3 = []
portfoliorepartition4 = []
portfoliorepartition5 = []
portfoliorepartition6 = []
portfoliorepartition7 = []
portfoliorepartition8 = []
portfoliorepartition9 = []
portfoliorepartition10 = []
portfoliorepartitionUnif = []

for i in range(0, nbactifsretenus):
    portfoliorepartitionUnif.append(1/nbactifsretenus)
for i in range(0, nbactifsretenus):
    portfoliorepartition.append(round(1/nbactifsretenus + (10 - i) * 0.005, 3))
for i in range(0, nbactifsretenus):
    if portfoliorepartition[i] < 0.012:
        portfoliorepartition[len(portfoliorepartition) - i] = portfoliorepartition[len(portfoliorepartition) - i] - 0.012 - portfoliorepartition[i]
        portfoliorepartition[i] = 0.012


portfoliorepartitions.append(portfoliorepartition)
portfoliorepartitions.append(portfoliorepartitionUnif)
print(portfoliorepartitions)
print(portfoliorepartition)



class Node(object):
    def gen_parent_list(self, parent):
        parentlist = parent.parentlist.copy()
        parentlist.append(parent.asset_id)
        return parentlist

    def add_child(self, obj):
        self.children.append(obj)

    def get_branch_list(self):
        return 'NOT IMPLEMENTED YET BUT VERY BASIC STUFF'

    def __init__(self, asset_id, proportion, parent=None):  # proportion or quantity?
        self.asset_id = asset_id
        self.proportion = proportion
        if parent is not None:
            self.parentlist = self.gen_parent_list(parent)
        else:
            self.parentlist = []
        self.children = []



def initialize_tree(treeIndexes, currentNode, depth, portfolios):
    base_proportion = 1 / 20
    # tree = Node(treeIndexes[0], base_proportion, None)
    # currentNode = tree
    parent = currentNode.asset_id;
    portfoliosandtree = [];
    global treescale
    for i in range(1, 20):
        if (not currentNode.parentlist.__contains__(treeIndexes[i])) and treeIndexes[i] != parent and depth < nbactifsretenus - 1 \
                and currentNode.children.__len__() < treescale:
            print("index is " + str(treeIndexes[i]) + " ")
            print(currentNode.parentlist)
            print("\n")

            newNode = Node(treeIndexes[i], base_proportion, currentNode)
            currentNode.add_child(newNode)
            initialize_tree(treeIndexes, newNode, depth + 1, portfolios)
        if depth == nbactifsretenus - 1:
            portfolio = currentNode.parentlist.copy()
            portfolio.append(currentNode.asset_id)
            portfolios.append(portfolio)

            print("portfolio is " + str(portfolio) + " and there are " + str(len(portfolios)) + " ready for process")
            print(treescale)
            break

    return portfolios


# Import actif data
treeIndexes = []


corelation_graph = np.loadtxt('trash/corellations363dl.csv', dtype='float64', delimiter=',', unpack=True)

#corelation_graph = np.loadtxt('coretmp.csv', dtype='float64', delimiter=',', unpack=True)

asset_datas = []  # contient une liste de moments donnés (croissants)
# avec ["ASSET_DATABASE_ID", "LABEL", "TYPE", "LAST_CLOSE_VALUE_IN_CURR", "SHARPE", "RENTABILITE", "ID"]
# corelation_graph = [[]]
for i in range(0, 13):  # à augmenter avec le nombre d'échantillons data
    asset_datas.append(pd.read_csv("resources/" + str(i) + ".csv"))

#faire bouger les noeuds en racine du portefeuilles
#faire bouger les
transformation_currency(asset_datas[0]['LAST_CLOSE_VALUE_IN_CURR'])

arg1=[292.0, 362.0, 323.0, 122.0, 224.0, 359.0, 152.0, 6.0, 22.0, 336.0, 21.0, 77.0, 207.0, 107.0, 237.0, 293.0, 327.0, 162.0, 316.0, 259.0]
arg2= transformpercent(arg1, [0.1, 0.078, 0.068, 0.085, 0.08, 0.075, 0.07, 0.065, 0.06, 0.055, 0.05, 0.045, 0.04, 0.035, 0.03, 0.025, 0.02, 0.015, 0.012, 0.012],asset_datas[0]['LAST_CLOSE_VALUE_IN_CURR'])
arg3=asset_datas[0]['LAST_CLOSE_VALUE_IN_CURR']
arg3bis = asset_datas[0]
check_validity_per_quantity(arg1, arg2, arg3)
submitPortfolio(arg1, arg2, arg3bis)

# Build tree

bestsharps = asset_datas[0].sort_values(by='SHARPE', ascending=False)['INDEX'].values
for i in range(0, 30):
    treeIndexes.append(bestsharps[i])
tree = Node(treeIndexes[0], 1 / 20, None)

portfolioIndexes = []
initialize_tree(treeIndexes, tree, 0, portfolioIndexes)


# Build echantillon data
#Pour le benchmark, ce sera une liste de liste de valeurs.
#On fait une liste temporelle de liste de valeurs de 1 à 364
# et on inverse les index i et j (on passe de 60 liste de 365 à 365 listes de 60)
auxbench = []
for i in range(0, len(asset_datas)):
    auxbench.append(asset_datas[i]['LAST_CLOSE_VALUE_IN_CURR'].values.tolist())
benchmark = transpose(auxbench)

print(len(portfolioIndexes))
list_portfolio = []
for i in range (0, len(portfolioIndexes)):
    list_portfolio.append(Pair(portfolioIndexes[i], portfoliorepartition))



rendementList = asset_datas[0]['RENDEMENT'].values


#for i in range (1, nbactifsretenus):
#    portfoliorepartition[i] = portfoliorepartition[i] - i * 0.05 #ou 5 selon si c'est en fraction ou en %





# PARTIE ARBRE


# Expli marko Romains
# Donner pour chaque ligne de l'arbre, [ [le nom, le pourcentage, la liste de jeu de données uniformes] ]
# maximiser le Sharpe
# variance du portefeuille
# minimiser les covariance
# 1) on a choppé les trois à la variance la plus basse
# 2)


#DONE rendement Romain already debugged
res = marcowitz_algo(list_portfolio, benchmark, rendementList, corelation_graph)
print("portefeuille final : " + str(res))

#def transformpercent(assets, proportions, data_on_assets): #dans data on s'interesse notamment à la valeur à l'instant t
#    print(data_on_assets)


def print_transform(list_portfolio) :
    for i in range (0, len(list_portfolio)) :
        assets = list_portfolio[i].first
        proportions = list_portfolio[i].second
        print(transformpercent(assets, proportions, asset_datas[0]['LAST_CLOSE_VALUE_IN_CURR']))

print_transform(list_portfolio)


"""dans data on s'interesse notamment à la valeur à l'instant t"""
#print_transform(list_portfolio)


#list_portfolio[0].first, list_portfolio[0].second
#submitPortfolio(res[0], [3.,3.,3.,3.,3., 3.,3.,3.,3.,3.], asset_datas[0])
smax = 0.
sharpemax = 0.
print(res)
for i in range (0, len(res)):
    submitPortfolio(list_portfolio[i].first, transformpercent(list_portfolio[i].first, list_portfolio[i].second, asset_datas[0]['LAST_CLOSE_VALUE_IN_CURR']), asset_datas[0])
    sharpejson = compute_ratios(20, asset_datas[0]['ASSET_DATABASE_ID'][101], None, '2012-01-02', '2018-08-31')
    print(sharpejson)
    sharpe = float(json.loads(sharpejson).get('1031', {})['20']['value'].replace(',', '.'))
    print(sharpe)
    #['20']['value'].replace(',', '.')
    if (sharpe > sharpemax):
        sharpemax = sharpe
        smax = i
submitPortfolio(list_portfolio[i].first, transformpercent(list_portfolio[i].first, list_portfolio[i].second, asset_datas[0]['LAST_CLOSE_VALUE_IN_CURR']), asset_datas[0])
print("ok")
print(portfoliorepartitions)

# PRIO 4 PARTIE ARBRE TODO renvoyer les listes des branches de plusieurs arbre et non seulement le premier afin d'avoir des référentiel priorité minimale
#TODO traduction de ligne en haut, build 30 (enfin nb actifs sharpés retenus) arbres avec noeud principal diff a chaque fois

# PRIO 1 TODO donner des différentes listes de proportion et non only (1/10, 1/10...)
# TODOROMAIN transformer les pourcentages en quantités d'actifs

# PRIO 3 TODO optimisation qui resuivra pour l'arbre
# PRIO 2 TODO check each actif's value
#TODO compute sharpe on best 20

# coefficient 0.05, n*
# PRIO 5 TODO après rendu construire graphique
